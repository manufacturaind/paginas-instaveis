# you had me at gmail

Para Silvio Meira, professor do Centro de Informática da Universidade Federal de Pernambuco, é "inconcebível" que a líder de um dos maiores países do mundo "fale com um telefone que não sabe de quem é nem a quê esse aparelho está submetido".

Meira afirma que o Brasil não avançou – e provavelmente até retrocedeu – em segurança da informação desde as revelações de Snowden. "De lá para cá, por exemplo, o sistema de e-mail de agentes de Estado que deveria ter mudado para sistema fechado, criptografado, continua igual. Continuamos mandando e-mail para agentes públicos em ministérios, em nível bastante alto, usando serviços como Gmail."

###### sexto dia da xepa