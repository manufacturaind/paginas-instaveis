# montaigne escreve sobre futebol

O Santos de Pelé, a seleção inglesa de 1966, a brasileira de 1970, a holandesa de 1974, e o Barcelona, base da seleção da Espanha nas conquistas do Mundial de 2010 e das Eurocopas de 2008 e 2012, revolucionaram o futebol, porque atuaram de maneira diferente do habitual da época e porque influenciaram transformações no estilo de se jogar em todo o mundo. Houve outros excepcionais times, mas não com a mesma importância.

As coisas vão e voltam, com algumas mudanças e nomes diferentes. Todo encontro é um reencontro com o que vivemos, imaginamos e/ou sonhamos.

###### nono dia da catilinárias