Qual é a relação que tem atualmente com as tecnologias?

Vivo num celeiro sem eletricidade, nas montanhas. Ocasionalmente uso o computador, mas ligado à internet por cabo. Não tenho nenhum aparelho sem fios que funcione através de micro-ondas.

###### vigésimo quarto da vitória de pirro