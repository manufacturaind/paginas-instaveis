# montaigne escreve sobre futebol

Críticos, de todas as áreas, costumam opinar mais pelo que gostariam de ver, pelo que imaginam e pelo ideal do que pelo que viram, pelo que se poderia fazer de diferente naquele momento e pela realidade. Existem comentaristas que têm sempre a mesma referência, a dos melhores instantes do futebol brasileiro. Temos de conhecer, de aprender com o passado, mas sem repeti-lo.

##### último dia da nessum dorma