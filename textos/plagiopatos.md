# Artista holandês acusa Fiesp de plagiar pato amarelo

O artista plástico holandês Florentijn Hofman acusa a Fiesp (Federação das Indústrias do Estado de São Paulo) de plagiar, em sua campanha contra aumento de impostos chamada "Não vou pagar o pato", a obra Rubber Duck (ou pato de borracha), exposta em São Paulo, em 2008, e em cidades como Amsterdã e Hong Kong.

A BBC Brasil entrou em contato com a fábrica de Guarulhos (SP) que produziu a obra para o artista holandês em 2008 e descobriu que se trata da mesma que tem produzido os patos em contrato com a Fiesp.

Denilson Sousa, dono da Big Format Infláveis, reconheceu que empresa produziu os dois patos e disse que a Fiesp enviou uma foto da obra do artista como "referência", mas que "nem sabe mais se tem o projeto de Hofman".

À BBC Brasil, a equipe de Hofman afirmou que a Fiesp transformou o projeto artístico original em uma "paródia política" e que o uso do desenho é "ilegal" e "infringe direitos autorais".

Procurada pela reportagem, a Fiesp negou as acusações de plágio e afirmou que a inspiração para os patos foram "patinhos de banheira", sem confirmar se enviou ou não o projeto holandês como referência.

###### nono dia da xepa