# montaigne escreve sobre futebol

Outra razão do sucesso do time inglês é a sequência de fatores aleatórios, conhecidos e desconhecidos, que formaram um ciclo, uma corrente positiva. Todos os raríssimos e extraordinários fatos que acontecem no mundo, em todas as áreas, belíssimos ou trágicos, decorrem de uma série imprevisível e cumulativa.

###### vigésimo sétimo dia da vitória de pirro