No dia 17 de março de 2006, pouco depois das seis da tarde, a privacidade de Francenildo dos Santos Costa foi exposta ao Brasil. Sob o título “Extratos revelam depósitos para caseiro”, o post que inaugurava o blog da revista Época divulgava um fato ligado a uma quebra de sigilo bancário ocorrida na véspera: “O caseiro Francenildo dos Santos Costa, que ganhou fama ao aparecer na CPI dos Bingos esta semana acusando o ministro Antonio Palocci de frequentar a ‘casa do lobby’, montada por lobistas de Ribeirão Preto, pode ser um trabalhador humilde […], mas está longe de passar por dificuldades financeiras…”


No dia 16 de março de 2016, pouco depois das seis da tarde, a privacidade de Dilma Rousseff foi exposta ao Brasil. Ouvia-se na GloboNews a conversa dela com o ex-presidente Lula, registrada por grampo telefônico horas antes: “Lula, deixa eu te falar uma coisa.” “Fala, querida.” “Seguinte, eu tô mandando o Messias junto com o papel pra gente ter ele, e só usa em caso de necessidade, que é o termo de posse, tá?” “Tá bom, tá bom.” “Só isso, você espera aí que ele tá indo aí.” “Tá bom, eu tô aqui, eu fico aguardando.”


Duas quebras de sigilo separadas por exatos dez anos. Duas reações distintas do governo liderado por um mesmo partido.


Dilma Rousseff declarou: “Não há justiça quando delações são tornadas públicas de forma seletiva para execração de alguns investigados e quando depoimentos são transformados em fatos espetaculares. Não há justiça para os cidadãos quando as garantias constitucionais da própria Presidência da República são violadas.” Francenildo dos Santos Costa é solidário: “A Dilma se sentiu violada, quebraram o sigilo dela.” Quem dera tivesse ele inspirado idênticas considerações. 