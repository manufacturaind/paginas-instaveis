# da brazilians

O que é que você faz na igreja?

Uh, só, uh – checando a drenagem

Eles doutrinam com canções e amizade

Ah, caras, fala sério, ela é feia

É como a minha irmã de 12 anos segurando uma arma

Você diz: o trabalho dela?

Soa como ficção científica, mas torna seus voos invisíveis

Todos pensavam que ele seria a chave pra coisa toda

Ninguém tem a chave pra porra nenhuma

Uma vez que eles perceberem que não têm tudo o que precisam, irão até o final da história

Clássica definição da esquerda não saber o que a direita faz

Você está trapaceando

Não é trapaça, é mágica

Bem, parece que quando eu e você matamos alguém deixamos provas

Eles não

Eles são melhores do que nós

Quem te disse isso? O pastor? Sua esposa? Pare com isso, pare de proteger essa laia

Esta é uma decisão estúpida e você não é estúpida

Você deveria me mostrar alguma porra de respeito caraio

Vocês não ajudam ninguém, nós é que te ajudamos

Você respeita Jesus mas não a gente?

Você não tinha escolha noite passada, isto é uma guerra

Ele me ensinou a lidar com esta situação

Ya, ele era um dos melhores

Ele era

Tenho saudades

Mas estamos juntos agora, você precisa confiar em mim

Carai, os brasileiros estão ficando mais cruéis a cada dia

Eles plantaram planos falsos pelo país e ficaram de butuca esperando que a gente escolhesse algum e botasse em prática

Isso deve ter dado um puta dum trabalho

E eu me pergunto – eles se importaram com tanta gente que teve de morrer? Esta administração, uh, as coisas que fizeram, isso me deixa passado

Ya, ya

Touré

Ya ba da ba du

Di ba la

Hu lu lu

Zabelê

Nana Shara

Lu la la

Pense sobre o que eles fizeram quando estiverem na sua porta amanhã e entrarem na sua casa

Limpa a geladeira

Quê? Me acordou pra limpar a geladeira?

Ya, limpa essa porra, e depois esfrega o chão e lava as toalhas

Que porra é essa?

Você quer ser adulta? Quer gastar seu dinheirinho pro que quiser? Ser adulta inclui fazer o que você não quer – e deixa eu te dizer: isso acontece o tempo todo

Vou renunciar à palhaçada, esta guerra se tornou muito pessoal pra mim

Posso ir até pra cadeia por causa de você

Se isso acontecer, vou te levar junto

Te levar daqui...

Ieh, te levar daqui

Ya, te levar daqui

Valeu

Valeu, valeu

Como dizemos no Brasil: cabeças vão rolar. E a sua será a primeira. Então agora você entende, meu problema é seu problema

O problema é o seguinte: não há problema

Se não há problema, resolvido tudo está

Eu prefiro resolver primeiro o problema dos outros

Decisão acertada

Eles são mestres em explorar a fraqueza dos outros

Todos temos fraquezas

Ya, ya

Touché

E eles vão achar as nossas

E as tuas

Nós vamos vasculhar tua vida, vamos atrás das tuas fraquezas, quaisquer uma, emocional, financeira, sexual

Vale tudo

Só não vale mulher com mulher

Vale também

Eu, eu...eu não tô confortável com isso

Isso é o Brasil

Não é a minha escolha, mas você vai ter de me dizer segredos – segredos que não contou a ninguém, porque é aí que o poder reside: nos segredos

Hoje foi um dia de merda, com meus filhos de merda, minha esposa de merda

Eu amo todos eles

Eu também, mas eles me tiram do sério

Não estou aqui pra ser salvo

Não sei se quero

Ninguém quer a salvação

Soa bom demais pra ser verdade

Temos de fazer uma escolha

Me desculpa

Você deve ter gente muito poderosa por trás pra nos trazer aqui num domingo

Estaremos mortos em cinco minutos

Isto cheira pior do que merda

Yeah

Tenha uma boa noite