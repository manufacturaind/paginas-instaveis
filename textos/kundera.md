# Kundera pra quem quisera

hoje, sei-o: em tempo de balanço, a chaga mais dolorosa é a das amizades perdidas; e não há nada mais estúpido do que sacrificar uma amizade à política.

[...]

O que mais me chocou nos grandes processos estalinistas, foi a fria aprovação com que os homens de Estado comunistas aceitavam a condenação à morte dos amigos. Porque eles eram todos amigos, ou seja, tinham-se conhecido intimamente, tinham vivido juntos momentos duros, emigração, perseguição, longa luta política. Como puderam sacrificar, e de maneira tão macabramente definitiva, a sua amizade?

Mas, tratar-se ia de amizade? Há uma relação humana para a qual, em checo, existe a palavra «soudruzstvi» (soudruh: camarada), isto é, «amizade dos camaradas»; a simpatia que une os que travam a mesma luta política. Quando desaparece a dedicação comum à causa, desaparece igualmente a razão da simpatia. Mas a amizade sujeita a um interesse superior à amizade não tem nada a ver com a amizade.

No nosso tempo, aprendemos a sujeitar a amizade àquilo que se chama as convicções. E mesmo com o orgulho da rectidão moral. De facto, é preciso alcançar uma grande maturidade para compreender que a opinião que defendemos é apenas a nossa hipótese preferida, necessariamente imperfeita, provavelmente transitória, que só os indivíduos muito limitados podem fazer passar por uma certeza ou uma verdade. Ao contrário do que sucede com a pueril fidelidade a uma convicção, a fidelidade a um amigo é uma virtude, porventura a única, a suprema.