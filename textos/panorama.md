# credenciais, per favore

Numa palavra, antes que você possa criticar ou opinar, todos os lados vão lhe pedir as credenciais.

Parte da direita decidiu que quem apoiou o PT não tem autorização para se indignar com o governo interino, porque aconteceu só que tomou posse o vice eleito com Dilma. O que você esperava?

Parte da esquerda decidiu que quem apoiou o impeachment não tem autorização para se indignar com o governo interino, porque está apenas colhendo o que muito claramente plantou. O que você esperava?

Parte da direita decidiu que quem é de esquerda, mesmo se se opunha ao PT, não tem autorização para se indignar com o governo interino, porque o PT é de esquerda. O que você esperava?

Parte da esquerda que se opunha ao PT decidiu que quem é de esquerda não tem autorização para se indignar com o governo interino, porque Dilma já havia feito alianças sacrílegas com as elites e o agronegócio, sendo para todos os efeitos um governo de direita. O que você esperava?

E assim por diante. Ciro Gomes não tem direito a opinar porque vem de uma família de políticos profissionais. Glenn Grenwald não tem direito a opinar porque é estrangeiro. Diogo Mainardi não tem porque mora na Itália. Paulo Brabo não tem direito a opinar sobre processos constitucionais porque já prescreveu que a anarquia é a única forma de governo. Luis Fernando Veríssimo não tem direito a opinar porque é escritor e sei lá, Lei Rouanet.

###### segundo dia de janus