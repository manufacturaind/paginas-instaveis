# montaigne escreve sobre futebol

 Barbara Heliodora, crítica teatral, que morreu nessa semana, dizia que escrevia para seus leitores, e não para artistas e autores. É um ensinamento para todos nós, analistas de esporte e de todas as áreas.

Nas décadas de 1950 e 1960, os técnicos eram discretos, orientadores, embora existissem também alguns estrategistas, como Tim. Ninguém se lembra de Lula, técnico do Santos de Pelé, talvez o maior time de todos os tempos. Ayrton Moreira, treinador do Cruzeiro, batia papo com os repórteres, enquanto fazíamos longos treinos coletivos. Dizem que o gordo Feola cochilava durante as partidas da Copa de 1958.

Com o tempo, por causa do avanço da ciência esportiva e da estatística, houve, progressivamente, uma supervalorização e uma vedetização dos treinadores. As análises das partidas passaram a ser feitas a partir dos resultados e da conduta dos “professores”.

Surgiram muitos treinadores formados na escola de educação física e que nunca foram atletas, como Cláudio Coutinho e Parreira. São técnicos que reconhecem a enorme importância da qualidade individual e da improvisação, mas que sonham em acabar com o acaso e fazer do futebol quase somente um jogo de lances ensaiados e planejados.

###### terceiro dia da origem