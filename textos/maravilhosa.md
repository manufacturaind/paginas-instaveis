Nas ruas o contraste é enorme, no lado verde-amarelo é um clima de Coreia do Norte, no outro é maracatu, samba, rock, funk, sem-terra, sem-teto, uma diversidade maravilhosa.

###### décimo sétimo dia da carbono 14