# montaigne escreve sobre futebol

Nos programas esportivos e em minhas caminhadas diárias, quando penso e converso com meus “fantasmas”, ouço muito que o futebol brasileiro perdeu sua essência. O que seria essa essência, a improvisação, o brincar com a bola? As grandes equipes da história, que ganharam ou não, uniam essas qualidades com a organização e a disciplina tática, de acordo com o estilo da época. É uma lenda, que, no passado, os grandes jogadores faziam o que queriam e ganhavam as partidas.

Como sou um psicólogo, um sociólogo de botequim, deduzo que as milhares de causas já ditas sobre a queda do futebol brasileiro convergem para a incapacidade técnica e para a falta de prazer, de obsessão, por fazer com excelência, cada dia melhor. Isso ocorre em todas as atividades e em todos os níveis. Fora as muitas exceções, predominam o desleixo, a pressa de fazer de qualquer jeito, de consumir, supervalorizar e descartar, quase sempre, com excessivo marketing e barulho. O silêncio incomoda.

##### sétimo dia de que país é esse?