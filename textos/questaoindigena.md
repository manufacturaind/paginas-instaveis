# a questão indígena

fala, delcídio: Eu conversava muito com Cardozo. Quando sabia de alguma operação, ele me dizia: 'Ventos frios sopram de Curitiba'. A senha era essa. Quando queria tratar de alguma ação de bastidores, ele mandava mensagem me convidando para falar da 'questão indígena', assunto recorrente do meu estado. Nós dois e Dilma falávamos sobre o tema às segundas-feiras, depois da reunião da coordenação política.

###### décimo oitavo dia da aletheia