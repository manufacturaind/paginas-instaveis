# montaigne escreve sobre futebol

No programa “Bate-Bola”, da ESPN Brasil, um telespectador me citou, entre vários outros ex-jogadores sugeridos para dirigir a CBF. Não tenho nenhuma vontade de ser dirigente nem preparo técnico de gestão. Sou, no máximo, um gestor de meus desejos e de minha vida.

###### penúltimo dia da passe livre