# ninguém quer sujar as mãos sozinho

Em palestra realizada na noite desta quarta-feira no Sesc da Esquina, na região central de Curitiba, o juiz Sergio Moro, que atua na Operação Lava Jato, afirmou que "sem a participação popular, a ação da Justiça é limitada".

O juiz, que é um estudioso da Operação Mãos Limpas, fez comparações entre a Justiça brasileira e a italiana. Lembrou que 40% dos condenados na Operação Mãos Limpas não cumpriram penas devido a prescrição de crimes ou alterações na lei.

Ele afirmou que o rito processual no Brasil é semelhante ao da Itália, e demonstrou preocupação com o fato. Para Moro, a contrarrevolução do mundo político no caso italiano, apesar do impacto da operação, impediu que houvessem mudanças significativas no combate aos crimes.

O juiz tem dúvidas se a situação será a mesma no Brasil ou se "vamos aproveitar esses momentos para melhorar nossas instituições para que esses casos não se tornem excepcionais no futuro". Moro acredita que ações contra casos de corrupção identificados deveriam ser rotina nos julgamentos da Justiça brasileira.

###### segundo dia da radioatividade