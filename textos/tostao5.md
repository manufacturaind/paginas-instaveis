# montaigne escreve sobre futebol

No “Bem Amigos”, do SporTV, a maioria dos comentaristas opinou que a seleção brasileira reagiu para empatar com o Paraguai, em 2 a 2, quando perdia por 2 a 0, porque o adversário foi todo para trás. Dunga e Gilmar Rinaldi, presentes no programa, retrucaram com veemência e disseram que o Brasil reagiu por causa da mudança tática e de jogadores feitas pelo técnico.

Penso que as duas coisas ocorreram simultaneamente, ação e reação, comum no futebol. Muitas coisas não se explicam, acontecem. Assim é também na vida.


###### último dia da vitória de pirro