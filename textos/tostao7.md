# montaigne escreve sobre futebol

Não há mais razão para definir a maneira de jogar pelo sistema tático desenhado na prancheta, já que os jogadores não param de correr, ocupam mais de uma posição e executam mais de uma função durante a partida. O sistema tático muda a cada momento e, algumas vezes, independe das orientações do técnico, embora parte da imprensa insista em explicar tudo o que acontece no jogo por uma sacada genial ou uma besteira cometida pelo técnico.

##### terceiro dia da acarajé