# grumpy

Tiririca, então, explicou: “Eu tô achando… eu tô achando realmente, na realidade, com sinceridade… Está totalmente do jeito que estamos vendo. O negócio tá feio. Tá feio porque ninguém sabe de nada, ninguém sabe quem é quem, ninguém sabe o que vai acontecer.”

Jô então quis saber: “O que falam pra você?” Tiririca entornou um copo de conhaque inteiro e mudou de assunto.

Mais para o fim da conversa, o palhaço falou um pouco de seu trabalho na Câmara dos Deputados: “Eu tô lá numa praia que não é a minha praia. Tem que ter pessoas pra orientar. Aí você fala: ‘Eu tava a fim de levar pra frente esse projeto’. Aí, eles elabora (sic), faz as coisas…”

Jô quis saber: “Fala um projeto que você tá levando à frente”. Tiririca, então, explicou: “Lá cada um levanta a sua bandeira. Eu levantei a bandeira do circo”. E deu detalhes sobre um dos projetos que apresentou.

Depois de 15 minutos de entrevista, Jô encerrou a conversa. “Já acabou? Eu não quero sair. Já tomei bem umas cinco doses”, disse Tiririca. E o apresentador, irônico, lembrou: “Já te dei mais tempo do que te deram lá, na hora da votação (do impeachment)”.

Tiririca (PR-SP) foi o deputado federal mais votado no país em 2010, com 1.353.820 votos. Ele se reelegeu em 2014 com 1.016.796 votos.

###### quarto dia de vício