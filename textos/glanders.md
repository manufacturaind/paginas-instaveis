# da brazilians

Estamos encrencados

Eu sei

Sempre esperei ter notícias de você

Nem tudo acontece como o planejado

Não

Eu gostaria que conversássemos antes disso

Eu era outra então 

Se acreditássemos em deus, diria para rezarmos 

Sempre há outra escolha, ninguém melhor do que você sabe disso

Não precisamos nos sentir como marcianos

Somos todos brasileiros agora

Só um bocadinho 

Vai ficar tudo bem

Você precisa falar com o pastor

Às vezes estou brincando, às vezes não 

Foi diferente, foi divertido 

Ela põe tudo a perder 

Ela é imprudente e piorou tudo para ela mesma

A crença de que ela precisa ouvir dele mais do que descobrir sozinha

Sua história não ter mais importância pra ninguém além dela própria ilustra as consequências de suas respectivas ações

Eles vão se ver muito pouco a partir de agora porque o trem tá quente 

Eu te disse que estava quente

Eu tava com fome

Eles tiveram que desistir dessa inocência 

Para gente tão inteligente não vai ser um dolce far niente 

Enquanto ela está num limbo os dois estão fudendo nos subúrbios 

O custo de manter o pastor vivo é aumentar os esforços para recrutá-la e treiná-la 

Completo candor é o único modo de lidar com essa merda

Ela pode voltar ao pastor como confidente, e deixar as coisas quentes

De fato, qualquer coisa pode ocorrer com qualquer um a qualquer momento e pouco disso é bom

É um problema de linguagem

Muito tempo investigando pra descobrir no fim que a tecnologia estava ultrapassada

Um pequeno techno thriller

Pobrezinha pobrezinha 

Yeah isso não parece bom pra ela

Ela está viva?

Só posso dizer que ela não está morta

A única questão é: ela sofrerá punição?

Mas isso é o suficiente?

As pessoas acreditam no que precisam acreditar

Não temos outra escolha aqui

E estivemos errados tantas vezes e estamos errados de novo, não aprendemos nada?

Eles nos deram uma saída que nos permite continuar nossas vidas

Isso é o Brasil, baby 

O que quer dizer? Estou voltando pra casa

É isso que te ensinam: que você pode sentir em vez de ver?

Eu te amo

Me diga na terça 

Saberemos na terça

Talvez talvez

Fui babaca, me perdoe

Você foi um cuzão

Acho que babaca é o suficiente 

Eu sei que você não faria nada, você é um homem difícil de encontrar, um homem bom, quanto a ela faz bem esse tipinho 

Ela acha que pegou pesado demais com você 

Não se executam pessoas por nada. Ela teve várias chances 

Seria ilegal fazer isso para ele, mas geralmente isso não impede as pessoas de fazerem isso em seriados na tv (nem na vida, suponho)

Ele não sabia que por trás dos panos ela estava olhando pela janela e o viu 

Se tiverem gravando, não importa - ele nunca voltará lá e ela sabe ser cuidadosa ao telefone 

Não vemos mais nada além de luz e sombra 

Eu amo essa escrita e a interpretação dele nos dá deliciosos momentos de ambiguidade 

Acho que o que você está fazendo é algo muito importante de ser feito

Ainda acha que seria melhor fugir?

Vamos ao meu apartamento, talvez eu tenha algo

Você sempre sonhou com Odessa

O que você disse a ela pela primeira vez?

Isso foi antes da guerra matávamos um ao outro todo dia 

Ele queria que eu soubesse que ela me amava 

Ninguém em sã consciência faria esse trabalho 

Ela faria eu seria normal 

Precisamos encontrar o Gabriel

A febre dele baixou ele está bebendo água mas precisará de tratamento por um tempo

Ele estava lá tentando não morrer de glanders 

É um luto terrível para todos nós 

Se é o que você quer, o que você sempre quis

Ninguém sabe mas é nossa melhor hipótese 

Esta é provavelmente a relação mais sincera e honesta que já tive com alguém

Do fundo do meu coração você sabe que eu menti

Sim

Desculpe eu não posso ser mais 

Nós não estamos indo embora 

Vocês viveriam perigosamente 

Mas não significa que não possamos nos divertir hoje 

Eles te ensinam a jogar?

Eu sinto que há toda essa grande conspiração 

Parte vital do treinamento 

É torturante não fazermos nenhum movimento quando tudo é incerto e somos pessoas de ação

Há um limite no progresso que você faz se não é honesto em sua própria vida

E qual a novidade?

Alguém se importa com você. Isto vai funcionar?

Quer beber?

Não valeu o cansaço bateu

Fique olhando o teto e em 8 horas tomarás o breakfest 