# epígrafes

Tudo é uma ficção. A realidade é Dunga.

Tostão



Algumas coisas temos que ler o que ninguém vê. Ver o que está atrás daquelas linhas e você começa a ser diferente dos outros.

Dunga



Cê sabe que deveria desviar disso e que isso não é bom procê, mas cê tá indefeso. Isso também - se cê pausar seu bom gosto por tempo o bastante pra entrar numa operação - é engraçado pacas. E isso não por causa do texto horrível, ou dos enredos complexos, ou da coisa-menos-desejável-de-abraçar-os atores cheios de queijo. Não, é a antecipação da próxima brilhantemente péssima piada ou fala de uma linha. 