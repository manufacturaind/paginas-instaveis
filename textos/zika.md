# zika

O cientista Amílcar Tanuri, uma das principais referências brasileiras em pesquisas sobre vírus, teve um computador furtado no Aeroporto Santos Dumont, no Centro do Rio. O equipamento armazenava dados inéditos de duas pesquisas sobre o vírus da zika. Em uma delas, a que investiga os efeitos de uma droga contra o vírus, não há cópia das informações e os experimentos terão de ser repetidos. Tanuri, chefe do Laboratório de Virologia Molecular da Universidade Federal do Rio de Janeiro (UFRJ), calcula que o prejuízo seja de US$ 5 mil, somente em insumos.

###### primeiro dia da repescagem