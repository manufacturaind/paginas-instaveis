# the basic con by lew welch

Those who can't find anything to live for,

always invent something to die for.




Then they want the rest of us to

die for it, too.




These, and an elite army of thousands,

who do nobody any good at all, but do

great harm to some,

have always collected vast sums from all.




Finally, all this machinery

tries to kill us,




because we won't die for it, too.


#### décimo quarto dia da carbono 14