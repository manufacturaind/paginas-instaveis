# montaigne escreve sobre futebol

O problema de se adotar um sistema tático e repeti-lo, sem variações, é tentar fazer com que todos os jogadores se adaptem a ele. Muitos não conseguem. Vou brincar de imaginar em que posições, no sistema atual 4-2-3-1, jogariam os maiores atletas do passado. Alguns não teriam problema. Já outros...

Garrincha conseguiria ser um ponta moderno, marcar e atacar? Dificilmente. Correria o risco de ficar na reserva, e não conheceríamos um dos gênios do futebol.

Se Gerson atuasse hoje, dirigido por um técnico mais atualizado e arejado, seria escalado de segundo volante (prefiro dizer meio-campista), por atuar de uma intermediária à outra, gostar de ficar com a bola, trocar passes, comandar o jogo, além de dar magistrais passes longos. Mas se o técnico fosse um ortodoxo, diria que armador habilidoso não pode ser volante. Teria de ser meia ofensivo. Gerson não seria o craque que foi.

Em que posição jogaria Pelé? Ao vê-lo treinar várias vezes, o técnico teria uma grande dúvida, se o escalaria de centroavante, por ser ele excepcional cabeceador, finalizador e pivô, se de atacante pelos lados, por ele ser extremamente veloz e habilidoso, ou como um meia mais recuado, pelo centro, por ser ele muito criativo e dar excelentes passes.

Se o auxiliar fosse irrequieto, desassossegado e conhecesse a história de nosso futebol, sugeriria ao treinador colocar Pelé de ponta de lança, formar dupla com o centroavante, com liberdade de voltar para receber a bola e de se movimentar por todo o ataque. O técnico olharia o auxiliar com uma pose de sábio e responderia: “Não dá. Ele tem de se adaptar ao sistema tático”.

Mesmo os gênios precisam estar no lugar certo, no momento certo e na época certa. O sucesso é a soma de talentos, acasos, desejos e oportunidades.

##### décimo primeiro dia da my way