# poesia numa hora dessas?

Em resposta à sua manifestação

Sob o n. 1.723/2016 protocolada

Na qual nos chamou atenção

O brilhante uso da nossa língua falada

Vimos pelo presente documento

Apresentar os resultados apurados

Na esperança de que com esse intento

Encerremos os seus desagrados


A fim de verificar

A situação apresentada

Pusemo-nos a vistoriar

As instalações internas afetadas

E eis que ficou constatado

A ocorrência de um vazamento

Comprovadamente sanado

No mais curto espaço de tempo

Para assegurar um fornecimento

Contínuo, lhano e escorreito

Fora instalado um equipamento

De precisão, em seu conceito,

E ao fim de 58h constantes

Registrou o importante instrumento

Não haver na rede pressões variantes

Que provocassem um derramamento


Desta forma, concluímos que o vazamento

Ocorrido em sua residência

Tratou-se de um fortuito acontecimento

Lamentável em sua essência

###### dia primeiro de vício