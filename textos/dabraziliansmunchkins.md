# da brazilians

Ao menos sabemos que você tem bons reflexos

Ela cozinhava bem?

Não sei, não pensávamos nestes termos

Ela era muito dura

É como as pessoas devem ser

Como você encontrou ela? 

Eu vi escrito “Militante” na bolsa dela enquanto lia meu livro

Você já fez com novinhos?

Nunca

Quêêê? Não se pode entender o Brasil sem provar os novinhos

Vou colocar isso na minha lista

Ele acha que estará pronto, na melhor das hipóteses, em poucos dias, na pior, em uma semana

Da última vez quase começamos uma epidemia

Será 10x pior

Que zika!

A melhor dissuasão é a força

Na sala da presidência não dizem isso

Você é que não ouviu

Ele está na cidade, disse que beberia com ele

Há uma palavra para o que você fez: arapuca

Quer se sentar?

Não faz diferença

Eu sei como o mundo gira e a lusitana roda, fiz uma gravação e dei ao meu advogado, está endereçada à justiça federal

Só quero dizer que te amo muito e o pastor também

Ele está sumido no Brasil

O Brasil é um país perigoso

Todo mundo sabe disso

Sabemos que [inaudível]

E você pensa o quê? Que sequestramos e matamos gente?

E-eu digo, no Brasil...ele desaparece? Isto soa muitcho loko pra mim

Eles são escrachados, cordiais, gente boa, você gostaria deles

Você gosta deles

Acho que sim

Você já passou pela pior parte

(suspiro) Não, a pior parte é sempre a próxima, depois disso eles jamais se recuperarão

Ei, eu ouvi um burburinho, vai ter um anúncio, eu não sei detalhes, mas ele está morto

Qu-quê? Tipo um...seilá, ataque cardíaco?

Não sei, não sei de nada

Às vezes o melhor movimento neste trem precisa de um cadim de sorte

Este cara andando por aí, nem imagino onde esteja, a não ser no Brasil, tentando alimentar os outros...

Um bom cristão

Esfregue o chão depois de terminar de lavar a louça

(suspiro) Ohhh

As pessoas desaparecem por todo tipo de razão, ela pode ter sido drogada ou raptada, você age como se ela fosse uma criminosa, ela é a pessoa mais honesta e leal do mundo

As pessoas mais decentes são as mais suscetíveis a esse tipo de manipulação

Sim, mas não ela, ela é esperta demais pra isso

Não subestime o poder das pessoas de serem estúpidas

Eu chequei a fita, não tem nada lá, ainda deve estar pelo Brasil

Tá falando sério? Você ouve o que diz? Olha, se você pudesse ouvir o que diz! Tudo o que você diz é loucura! Brasil? Nunca estive no Brasil! E o quê, agora vamos todos falar brasileiro? O que vamos fazer lá? Não se pode ser brasileiro no Brasil! E então o que vamos fazer? Em que país? Pra quem vamos mentir? Com quem vamos sumir?

Certo, querida, relaxa

É isso o que tem a dizer?

Relaxa e goza

De repente tudo fez sentido na minha vida

Sacumé, é tudo tão...intenso

Quer açúcar ou adoçante?

Não, apenas cafeína basta

Vou te contar um segredo [inaudível]

Ele tem outro trabalho então

Tenho de checar a esposa do pastor

Escuta, a coisa mais foda no nosso trabalho é o lance da confiança, nunca sabemos com certeza se os outros estão nos dizendo a verdade, mas uma coisa que eu e ele temos é que sempre dizemos um ao outro a verdade e a partir de agora você é parte disso, não podemos dizer todos os detalhes do nosso trabalho, mas iremos dizer o que pudermos e não vamos mentir a você

Não devemos perguntar a ela sobre a gravação agora

Não quero que ela pense...

Sim

É melhor esperar

Soa mais inteligente assim

Veremos com o Gabriel se pode fazer algo por nós

Isto se ele estiver vivo

Tem planos pra amanhã?

Não

Quer tomar algo?

Beleza, mas só um pouquinho, tenho de ir pra igreja

Ótimo

Valeu