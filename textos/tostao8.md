# montaigne escreve sobre futebol

Impressiona-me como quase todas as regras continuam atuais e corretas, já que foram definidas em 1863, na criação da Liga Inglesa, oficialmente a primeira do mundo. Contam que tudo foi resolvido em um pub, com muita cerveja. Duas sábias decisões foram tomadas, o tamanho do campo e o número de jogadores de cada lado. Por causa da enorme velocidade atual, muitos acham que o campo ficou pequeno. Discordo. Penso que antes é que era grande. Havia enormes espaços.

Porque 11? A turma da cerveja deve ter pensado que, além do goleiro, haveria cinco duplas em cada time, formada por dois zagueiros, dois laterais, dois médios (volantes), dois meias e dois atacantes. Tudo simétrico. O goleiro ficou sozinho, solitário. Deve ser por isso que existe a fama de que todo goleiro é esquisito, exibicionista.

A simetria é uma obsessão do ser humano desde as cavernas. Seria uma expressão da dualidade humana, dividido entre o bem e o mal, entre a razão e a paixão, entre o princípio do prazer e o da realidade. Seria uma maneira de dominar os instintos e de conciliar os opostos.

###### décimo dia da aletheia