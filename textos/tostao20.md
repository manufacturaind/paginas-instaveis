# montaigne escreve sobre futebol

A evolução no futebol, nos últimos anos, tem sido diminuir a importância do jogador que atua em pequenos espaços, embora, às vezes, eles sejam decisivos, e valorizar mais os que fazem mais de uma função, desde que tenham talento. Não adianta correr sem pensar. Mas não se deve confundir os jogadores que, durante um jogo, fazem mais de uma função com os que atuam em várias posições, em partidas diferentes.

Quando o atacante Rooney, um ótimo finalizador, volta para receber a bola em seu campo, para, depois, chegar ao ataque, não significa que ele possa fazer, com a mesma eficiência, o sentido inverso, ou seja, atuar de volante, para, depois, chegar à frente, como tem sido escalado por Van Gaal, no Manchester United. Rooney, por ser um jogador inteligente, joga bem também de volante, mas não é seu lugar. Isso o prejudica e ao time. Os moderninhos adoram elogiar o técnico que muda a posição dos jogadores, mesmo sem motivo, como se isso fosse moderno.

Tenho muita admiração pelo criativo Guardiola, mas discordo quando ele exagera em suas invencionices, ao colocar dois excepcionais laterais, Alaba e, especialmente, Lahn, no meio-campo ou na zaga, como, algumas vezes, joga Alaba. Pior, os laterais escalados são fracos para o nível do Bayern. Lahn está contundido. O técnico da seleção alemã fez o mesmo na Copa, percebeu o erro, voltou Lahn para a lateral, e o time ficou muito melhor.

Mourinho, o “number one”, apelido dado por ele próprio, improvisa o lateral-direito Azpilicueta, apenas um bom marcador, pela esquerda e deixa na reserva Filipe Luís, muito bom na marcação e no apoio. Dunga está certo em escalá-lo na seleção, mas erra ao não convocar Marcelo, uma excelente opção, em algumas situações, por ser um excepcional apoiador.

Ancelotti não tem a fama de Mourinho nem de Guardiola, mas é um técnico que executa muito bem tudo o que é essencial, no momento certo.

Entre vários motivos, critico os técnicos brasileiros por terem, durante os últimos tempos, contribuído para a queda do nosso futebol, mas colocar jogadores nos lugares errados não é uma de suas deficiências. Além disso, muitos têm evoluído. De vez em quando, vejo equipes com ótima qualidade coletiva. É preciso que isso se torne quase uma rotina.

##### quarto dia de my way