When Brazil’s best loved clown Tiririca was elected to congress in 2010 there was much gnashing of teeth among commentators about what his victory said about the direction of the country’s politics.

But on Sunday as the lower house voted on whether to impeach President Dilma Rousseff over accusations she broke budgetary laws it was clear that Tiririca not only fits right in but is far from the biggest joker in the political pack.

In the legislature’s most important session in almost a quarter of a century many of its members conducted themselves with all the decorum of drunken football fans at a local derby.

###### sétimo dia da vitória de pirro