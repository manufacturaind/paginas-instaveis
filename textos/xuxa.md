# reunião de celebridade com ministro da educação é tradição

Um selfie publicado no perfil oficial do ministro da Educação, Cid Gomes, no Facebook  tem rendido críticas ao dirigente da pasta na manhã desta quinta.

A foto, publicada na noite de quarta às 22h34, teve mais de 14.300 curtidas (likes) e mais de 3.600 compartilhamentos.

Na rede social, o ministro escreveu: "Estive com a Xuxa hoje. Ela tem produzido excelentes materiais para o Ensino Infantil e se dispôs a colaborar com dois importantes projetos do Governo Federal: o Mais Creches e o Pacto Nacional de Alfabetização na Idade Certa".

Entre os mais de 1.600 comentários na postagem, o tom de crítica é mais frequente, como é o caso de Luciana Zenha. Ela escreveu: "Eu sou professora e o que me entristece é ver um Ministro valorizar este ícone falido da mídia no Brasil e não reconhecer e apoiar grandes educadores e pesquisadores brasileiros. Volta (sic) Paulo Freire!".

Outra internauta questiona o elogio de Cid Gomes em relação ao material que a equipe da apresentadora produz, pedindo que os educadores sejam valorizados. A internauta Raquel La Corte dos Santos postou: "Senhor Ministro, por favor, converse com educadores de verdade. Que brincadeira de mal gosto é essa? Muitos educadores poderão não ser "bonitos" dentro do padrão televisivo mas terão a beleza que a educação precisa: conhecimento de causa, engajamento social, seriedade, senso do coletivo e foco no humano e não no marketing". 

###### oitavo dia de my way