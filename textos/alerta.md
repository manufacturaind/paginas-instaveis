Eu alertei que estávamos indo em direção a um caos político. E por quê? Porque o governo havia perdido o contato com a sociedade. Como um governo eleito com 54 milhões de votos poderia estar executando a mesma agenda do partido de oposição? As pessoas não conseguiam entender aquilo.

Eles tinham que ter ido a público e dizer "vejam, nós cometemos um erro, estávamos errados". Sem fazer este mea culpa, por que a população daria um voto de confiança à presidente Dilma Rousseff? Este foi o momento-chave em que o governo se perdeu, perdeu popularidade e perdeu sua credibilidade com a população.

##### oitavo dia da vitória de pirro