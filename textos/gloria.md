# glória pires, digo, bernardo soares:

Ter opiniões é estar vendido a si mesmo. Não ter opiniões é existir. Ter todas as opiniões é ser poeta.

##### décimo dia da acarajé