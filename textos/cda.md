# aí pelas três da tarde de maio

Não há nunca testemunhas. Há desatentos. Curiosos, muitos.

Quem reconhece o drama, quando se precipita, sem máscara?

Se morro de amor, todos o ignoram

e negam. O próprio amor se desconhece e maltrata.

O próprio amor se esconde, ao jeito dos bichos caçados;

não está certo de ser amor, há tanto lavou a memória

das impurezas de barro e folha em que repousava. E resta,

perdida no ar, por que melhor se conserve,

uma particular tristeza, a imprimir seu selo nas nuvens.

###### terceiro dia de vício