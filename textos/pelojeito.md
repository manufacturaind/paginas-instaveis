# pelo jeito...

Em seu acordo de delação premiada, o ex-líder do governo Delcídio do Amaral também apontou o atual presidente do PSDB, Aécio Neves, como destinatário de propina em um esquema de corrupção na hidrelétrica de Furnas. Delcídio não citou valores que teriam sido repassados ao senador tucano, mas disse que o então diretor de Engenharia de Furnas Dimas Toledo, que ocupava o cargo apoiado pelo PP e por Aécio, tinha um "vínculo muito forte" com o senador do PSDB.

Às autoridades responsáveis pela Operação Lava Jato, Delcídio revelou que "Dimas operacionalizava pagamentos e um dos beneficiários dos valores ilícitos sem dúvida foi Aécio Neves, assim como também o PP, através de José Janene". "O próprio PT recebeu valores, mas não sabe ao certo quem os recebia e de que forma", disse o delator.

Em uma viagem em maio de 2005, o senador Delcídio, que hoje anunciou sua desfiliação do PT, relatou que o então presidente Lula quis saber quem era Toledo e então disse: "Eu assumi e o Janene veio pedir pelo Dimas. Depois veio o Aécio e pediu por ele. Agora o PT, que era contra, está a favor". Na sequência, Lula concluiu: "Pelo jeito ele está roubando muito".

###### décimo segundo dia da aletheia