# Suave mari magno

Lembra-me que, em certo dia,

Na rua, ao sol de abril,

Envenenado morria

Um pobre brasil.



Arfava, espumava e ria,

De um riso espúrio e bufão,

Ventre e pernas sacudia

Na convulsão.



Um bando sem fim de bozos

Divertia-se a foder,

Infeccioso,



Junto ao brasil que ia morrer,

Como se lhes desse gozo

Ver padecer.