# quem paga o pato

O gramado da Esplanada dos Ministérios amanheceu tomado por milhares de patos de borracha nesta terça-feira. Cerca de cinco mil exemplares do símbolo do movimento “Não Vou Pagar o Pato”, da Federação das Indústrias de São Paulo (Fiesp), foram colocados no local em protesto contra a corrupção e a favor do impeachment da presidente Dilma Rousseff (PT).

O mascote é conhecido das manifestações anti-governo na Avenida Paulista, em São Paulo. Os manifestantes pretendem ainda, inflar outro pato de borracha de cerca de 20 metros de altura no gramado central da Esplanada. Os patinhos devem permanecer no local por pelos menos 10 dias.

O movimento “Não Vou Pagar o Pato”, lançado em outubro de 2015, quando representantes da Fiesp também colocaram centenas de patos de borracha no espelho d’água em frente ao Congresso Nacional, em Brasília. Eles defendem a derrubada do projeto do governo de implantação da Contribuição Permanente de Movimentação Financeira (CPMF) e é apoiado por dezenas de organizações empresariais, entidades da indústria, do comércio, dos serviços e da agricultura.

###### oitavo dia da xepa