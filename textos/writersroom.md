# writer's room

Tão exitoso quanto o Brasil era, se você pudesse de alguma forma viajar no tempo e cair na era da mídia social, tudo que envolve o Brasil realmente é um acelerador para o tipo de conversa que todos estão procurando ter

A execução foi tirada, passo a passo, daquele livro 

Estaríamos felizes se tivéssemos conseguido executar o plano na última temporada. Não conseguimos. Levamos mais tempo do que esperamos para encerrar essa história

Não chegamos lá, não chegamos lá e não chegamos lá. Isto é nosso, sem tirar nem pôr.

Sabíamos que seria uma longa história. Pois tem a ver com a consequências de nossas ações

Não se importe se a audiência pode seguir todos os detalhes ou motivações profissionais das várias operações, desde que o contexto emocional disso faça sentido no plano geral

Entre num beco e descubra como sair, diz a sabedoria dos narradores 

Os editores devem “construir e reconstruir cada momento emocional” fazendo seu primeiro corte do episódio a "peça final do quebra-cabeça da história"

Não temos de dizer tudo a todos desde que seja interessante. Não vamos bancar a babá

Há algo de transcendente nisto. Ela transcende a si própria e se torna uma pessoa diferente por causa de sua relação com ele. Ela para de se preocupar apenas com sua própria sobrevivência e começa a se preocupar com sua própria alma. De certo modo, o que poderia ser mais romântico do que isto?

Ela não tinha mais escolhas. Quantas chances mais você daria a ela? Eles foram razoáveis, justos, deram mais chances extras do que alguém esperaria. É exatamente o que você diz: poderia começar a parecer uma piada se durasse pra sempre. Se liga, eles poderiam ter dado um balaço na cabeça dela muito tempo atrás, antes que ela crescesse como um ser humano

O que você queria: que a cena não fosse escrita e acontecesse fora da câmera? Não podemos mudar a realidade das coisas

Mas tinham de fazer isso?

Oh, sem dúvida 

Não matá-la teria rebaixado tudo que foi feito pra criação de engajamentos e realismo 

Muito do que escrevemos, se este fosse um show ao vivo, penso que poderíamos nos sentir indulgentes ou melosos demais, ou então cínicos só pelo prazer de sermos cínicos, mas porque você tem esses personagens caricaturais dizendo todo tipo de coisa, isto abre seu coração um bocadinho