# montaigne escreve sobre futebol


Assim como a vida, as relações humanas e as reações emocionais, o futebol é muito complexo. O ser humano, principalmente os operatórios e os utilitaristas, é que tentam simplificá-lo, controlá-lo, com racionalizações, pré-conceitos, excesso de regras, de estatísticas e de tentativas de achar uma única razão que explique tudo, até o inexplicável e o incontrolável.

###### vigésimo segundo dia de my way