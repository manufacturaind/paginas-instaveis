João Santana aprendeu a enrolar os outros em duas profissões que ensinam isso muito bem: o jornalismo e a música. Se formou em jornalismo em Salvador, mas fez seu nome primeiro como letrista do grupo Bendegó. Santana assinava "Patinhas", apelido de quando foi tesoureiro de centro acadêmico. Foi o principal parceiro de Gereba, fundador do Bendegó, e participou de seis discos do grupo. 

Depois Patinhas trocou a música pelo Jornal da Bahia, e O Globo, Veja, Jornal do Brasil e finalmente Isto É. Virou João Santana e jornalista premiado. Na Isto É deu grande contribuição à nação e não dá para discordar: entrevistou o motorista Eriberto França, matéria que ajudou a derrubar Fernando Collor, que era o presidente. Santana faturou um Prêmio Esso pelo feito.

Como essa figura virou o mago das eleições, o cara que reelegeu Lula no auge do mensalão, Dilma que ninguém conhecia, Haddad idem? E presidentes em Angola, República Dominicana e Venezuela (Chávez / Maduro)? E, caramba, trabalhou nas campanhas de Celso Pitta e Antonio Palocci?
No alvo de investigações da Lava-Jato, Santana tem muito o que explicar. Dinheiro de origem suspeita (mas que verba de campanha política nesse país não é?).

###### primeiro dia da acarajé