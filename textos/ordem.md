# a formalidade diz da posição em que estão os brasileiros e as brasileiras

Congresso Nacional, Brasília-DF, 1º de janeiro de 2011

Senhor presidente do Congresso Nacional, senador José Sarney,

Senhores chefes de Estado e de Governo que me honram com as suas presenças,

Senhor vice-presidente da República, Michel Temer,

Senhor presidente da Câmara dos Deputados, deputado Marco Maia,

Senhor presidente do Supremo Tribunal Federal, ministro Cezar Peluso,

Senhoras e senhores chefes das missões estrangeiras,

Senhoras e senhores ministros de Estado,

Senhoras e senhores governadores,

Senhoras e senhores senadores,

Senhoras e senhores deputados federais,

Senhoras e senhores representantes da imprensa,

Meus queridos brasileiros e brasileiras

###### A.L-V