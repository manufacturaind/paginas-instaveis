# sapo

COLLOR: O governo é mais ou menos como aquela figura que contam do sapo. Que se coloca numa tigela de água em cima de uma fonte de energia de fogo, num bico de gás, e o sapo está dentro da água. E a água vai esquentando, esquentando, esquentando, e o sapo fica aguentando tudo aquilo até que a água ferve e o sapo não sai dali. Quando você coloca o sapo dentro da água fervendo, o sapo pula imediatamente. Mas quando ele está dentro da água e a água vai esquentando aos poucos, ele não se apercebe do perigo que está correndo. Então, acho que esse é mais ou menos o problema que esse governo vem passando. Ele [governo] não está escutando, não é de agora. Ele não está escutando já de algum tempo.

###### terceiro dia da nessum dorma