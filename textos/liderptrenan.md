# nada demais

Diferente do bombardeio feito à gravação da conversa do ex-ministro Romero Jucá (PMDB-RR), o líder do PT no Senado, Paulo Rocha (PA), disse que não há nada de mais na conversa gravada pelo ex-presidente da Transpetro, Sérgio Machado, com o presidente do Senado, Renan Calheiros (PMDB-AL). Os tucanos, por sua vez, criticaram as gravações.

Para o líder petista, cabe ao presidente do Senado conversar com presidentes de outros poderes para definir caminhos para o país, e faz parte de suas atribuições melhorar a legislação, como o caso da lei do impeachment.

"Diferente da outra gravação com Jucá, não há crime nenhum na conversa do presidente Renan Calheiros. Ele já tinha falado em entrevista sobre a necessidade de melhorar a legislação, como a caduca lei do impeachment, de 1950. É uma responsabilidade dele, como presidente do Congresso, adaptar a legislação em função do momento político. Não tem a gravidade da conversa do Jucá", defendeu o líder do PT, senador Paulo Rocha.

###### segundo dia de vício