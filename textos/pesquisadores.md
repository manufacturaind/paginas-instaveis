# aponta estudo

A ferramenta digital criada pelos pesquisadores mapeia, de hora em hora, todas as reportagens publicadas pelos 117 veículos de comunicação selecionados e verifica seus compartilhamentos a partir de um sistema oferecido pelo próprio Facebook.

Além do levantamento digital, os pesquisadores fizeram pesquisas de opinião em protestos realizados tanto por grupos ligados à direita, quanto à esquerda, na avenida Paulista, em São Paulo.

"Cada lado dessa disputa construiu narrativas mais ou menos simplistas para defender suas posições. Tanto os boatos como as matérias produzidas foram muito compartilhados quando se adequaram a essas narrativas", explica Marcio Moretto.

"Uma das narrativas de ambos os grupos foi de que o outro era formado pelos verdadeiros corruptos."

O professor diz que as pesquisas também mostraram que ambos os lados da disputa do impeachment "eram propensos a acreditar em boatos que confirmavam suas narrativas pré-estabelecidas".

Ele dá exemplos de mitos citados pelos dois lados nas ruas: "'Lulinha é sócio da Friboi', de um lado, e 'Sergio Moro é filiado ao PSDB', de outro".

Para o pesquisador, a popularidade dos boatos tem a ver com a maturidade dos usuários de redes sociais.

###### sexto dia da vitória de pirro