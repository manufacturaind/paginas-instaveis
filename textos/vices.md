# vices

Após o afastamento da presidente Dilma pela abertura do processo de impeachment no Senado, Michel Temer tomou posse, nesta quinta-feira, como presidente interino do país. Na história da República brasileira, ele não é o primeiro vice a chegar ao posto máximo do poder Executivo: antes de Temer, sete outros vices assumiram — quatro após morte dos presidentes, dois por renúncia e um por impeachment.

O primeiro presidente do Brasil, Marechal Deodoro da Fonseca, foi também o primeiro a passar o cargo para o vice, Floriano Peixoto, após renúncia do poder, em 1891. Menos de vinte anos depois, em 1909, outro vice-presidente, Nilo Peçanha, assumiu o mandato no lugar de Afonso Pena, que morreu. A história se repetiu em 1918, com a morte do então presidente Rodrigo Alves e a posse de Delfim Moreira.

A instabilidade política após o suicídio de Getúlio Vargas, em 1954, não deixou nem o vice terminar o mandato: Café Filho alegou motivos de saúde para se afastar da presidência, abrindo o cargo para o próximo na linha sucessória, Carlos Luz. Governando por apenas alguns meses, o “vice do vice” também não aguentou as pressões políticas e foi deposto, e quem assumiu o mandato foi Nereu Ramos, então vice-presidente do Senado.

Primeiro presidente empossado em Brasília, Jânio Quadros teve um mandato de curta duração: abdicou do cargo após denúncias de um suposto golpe de estado. A posse de seu vice, João Goulart, o “Jango”, foi questionada e até novas eleições foram cogitadas. Com o golpe militar, em 1964, Jango foi deposto e teve seus direitos políticos cassados.

A história recente da democracia brasileira, pós ditadura militar, também relembra dois vice-presidentes que assumiram. José Sarney chegou à presidência, em 1985, após a morte do presidente eleito indiretamente, Tancredo Neves, que nunca chegou a assumir. Em 1992, o Senado aprovou o impeachment do então presidente Fernando Collor, que depois veio a renunciar, e quem foi alçado à presidência foi o vice Itamar Franco, que lá permaneceu até 1994.

Temer continuará na presidência até o julgamento de Dilma ser concluído no Senado, o que pode demorar 180 dias. Se Dilma for condenada, Temer permanece como presidente até as próximas eleições, em 2018.

###### trigésimo primeiro dia da vitória de pirro