# fala, tio rei!

À medida que o “mestre”, “o professor” e o “guia” vai enlouquecendo — e os sinais de paranoia e alheamento da realidade vão se tornando a cada dia mais evidentes; a questão é de química cerebral mesmo —, seus súditos intelectuais tentam emular o guru. Como a estes faltam as referências que o outro consegue manejar com certa destreza, apesar da discricionariedade e das idiossincrasias, sobra aos meliantes só o gosto pela grosseria, pelo baixo calão, pela vulgaridade.

Olavo se tornou servidor de uma candidatura à Presidência. Sim, a de Jair Bolsonaro. Espero que esteja ao menos sendo bem remunerado por isso. Conhecemos, no Brasil, o fenômeno dos “blogs sujos”. Olavo é o “filosofo” — e coloquem-se aspas aí — sujo. Se chegou a pensar com autonomia, em algum momento, e duvido crescentemente disso, esta desapareceu.

O que tirou Olavo da casinha? Respondo: a sua brutal incapacidade de ler a realidade.

O “filósofo” jamais imaginou que Dilma sofreria um processo de impeachment. Tentou ser o condutor das massas nas jornadas de 2015 e 2016, mas ninguém sabia quem era ele, a não ser os feios, sujos e malvados de sempre. Os que seguiam as suas ideias só serviam para difamar um movimento que tinha e tem a democracia como valor inegociável.

Olavo de Carvalho, o bobalhão, e os bolsonaretes queriam “intervenção militar”. Não confiavam que o processo político — e democrático — se encarregaria de depor Dilma Rousseff. Eram entusiastas dos coturnos saneadores passando sobre Brasília “por um breve período”. Lixo!

##### trigésimo sexto dia da vitória de pirro