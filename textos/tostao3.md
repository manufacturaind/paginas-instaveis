# montaigne escreve sobre futebol

Não existe vida sem ordem, organização. Os mais inventivos também precisam de regras para criar. Existe ordem até no caos. O perigo é o excesso de regras, que podem inibir a imaginação e a improvisação.

###### décimo quinto dia da triplo x