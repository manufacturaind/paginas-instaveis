# montaigne escreve sobre futebol

Uma partida de futebol pode ser vista de várias maneiras. Uma delas, como uma disputa esportiva, de habilidade, técnica, tática, criatividade, força física e emoção. Outra, como um espetáculo lúdico, teatral, literário, onde estão presentes os sentimentos e as contradições humanas.

Dizem que Nelson Rodrigues não entendia de futebol, não enxergava bem e, no fim dos jogos, no Maracanã, saía com Armando Nogueira e perguntava como havia sido a partida, quem tinha sido o herói, para criar seu personagem da semana. Ele ia para a redação e escrevia belíssimos textos, com numerosas metáforas e deliciosos exageros. E ainda ironizava os “entendidos”, que tentavam explicar o resultado pelos detalhes técnicos e táticos.

Todos os pensadores que escrevem nos jornais, sobre a vida e sobre o comportamento humano, deveriam falar mais de futebol. Eles, por escreverem muito bem, não terem os vícios, os chavões e o futebolês, enriqueceriam a crônica esportiva.

Como sou um filósofo e psicólogo de botequim e metido a entender de futebol, tento ter os dois olhares. Eles, com frequência, não se combinam. Um quer saber mais e ser mais importante do que o outro.

Recentemente, um leitor disse que, ultimamente, tenho tentado explicar demais os detalhes técnicos e táticos. Deve ser porque estou preocupado com o 7 a 1 e com a queda do futebol brasileiro. Quando mais penso, menos entendo.

Existem muitas razões e discussões sobre a diminuição de talentos no Brasil. A formação de bons jogadores não se reduziu. O que me intriga é a falta dos excepcionais, os que estão no nível intermediário entre um Neymar, que surge sem aplicativos, e o grande número de bons jogadores, espalhados pelo Brasil e pelo mundo. Porque, em vez de um Tardelli, não temos um Suárez? No lugar de um William, um Hazard? Em vez de um Oscar, um Iniesta? No lugar de um Fernandinho, um Kroos?

Muitos técnicos e comentaristas dizem que os jovens de mais talento precisam aprender a jogar coletivamente, a ter os mesmos deveres e obrigações dos outros e que só vão brilhar se atuarem em times com ótimo conjunto. Isso é óbvio, essencial. Mas não significa que eles têm de treinar da mesma maneira que os outros e serem colocados em uma forma de produção em série, para exportação. Somos todos diferentes.

Quando jogava no juvenil do América, eu, que era apenas uma promessa e que, sem nenhuma falsa modéstia, me tornei apenas um excelente jogador, longe dos grandes da história, tinha uma atenção diferente. O técnico Biju me falava que eu deveria treinar junto com todos, tentar melhorar minhas virtudes e corrigir minhas deficiências, mas que, se eu quisesse ser um craque, deveria treinar o que sabia fazer muito bem, separado, exaustivamente, para fazer ainda melhor.

O mundo e o futebol ficariam sem graça se não existissem os diferentes, se houvesse apenas um sistema tático, um único tipo de jogador para a mesma posição, uma única emissora para transmitir uma partida tão esperada, uma única visão filosófica, psicológica, um único estilo de vida e um único tipo de sorvete de coco.

###### sétimo dia da pixuleco