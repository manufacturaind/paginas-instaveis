# confeitaria

![](http://koizo.org/paginas-instaveis/images/cunhaconfeitaria.jpg)

a proclamação da República, longe de ser a profunda transformação social e política afirmada pelos propagandistas e revolucionários com mais entusiasmo do que razão e mais ingenuidade do que realismo, era apenas uma mudança de tabuleta — a confeitaria continuava a mesma

###### primeiro dia da vitória de pirro