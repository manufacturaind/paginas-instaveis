# montaigne escreve sobre futebol

Os perigos do otimismo são distorcer a realidade, se tornar excessivamente utilitarista e contentar-se apenas com a vitória. Não basta vencer, é preciso encantar. O futuro do futebol depende desse fascínio.

###### décimo dia de que país é esse?