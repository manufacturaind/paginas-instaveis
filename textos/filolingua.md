# filosofia da linguagem

Estamos dirigindo (...) num Fiat 147; a porta se abriu e X, aliás Brasil, caiu nos trilhos. Puxamos Brasil pra cima, que então disse: "alguém caiu?" (...) Quando foi dito que Brasil caiu, Brasil disse: "Brasil caiu? Pobre Brasil!"

###### vigésimo quinto dia da vitória de pirro