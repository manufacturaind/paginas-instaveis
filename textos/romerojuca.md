# os líderes de governo de lula e dilma

Nascido no Recife, Romero Jucá, 60 anos, está, atualmente, em seu terceiro mandato consecutivo como senador por Roraima, Estado pelo qual foi governador entre 15 de setembro de 1988 e 31 de dezembro de 1990. Passou pelo PSDB, onde foi vice-líder do governo Fernando Henrique Cardoso, antes de filiar-se ao PMDB. Entre março e julho de 2005, foi Ministro da Previdência Social, mas, por conduta suspeita de corrupção com empréstimos bancários irregulares, foi exonerado poucos dias depois. Em 2006, foi escolhido líder do governo do presidente Luiz Inácio Lula da Silva, cargo que também ocupou na gestão da presidente Dilma Rousseff. 

