# montaigne escreve sobre futebol

Hoje, o futebol vive outro período marcante, o do desenvolvimento ainda maior da ciência esportiva, principalmente da estatística. Todos os grandes times do Brasil e do mundo possuem equipes de informática, chamados de analistas técnicos e táticos, especializados em futebol, que fornecem aos treinadores todos os dados sobre os atletas. Isso ajuda os técnicos a tomarem decisões e os clubes nas contratações. Será que os analistas técnicos e táticos, no futuro, vão tomar o lugar dos treinadores? Situações semelhantes ocorrem em quase todas as profissões.

Existe atualmente uma adoração aos números e à tecnologia, que, às vezes, chega à ingenuidade e ao desconhecimento do ser humano. São os mesmos que têm um grande descaso pelo acaso, como se a aceitação do imprevisível fosse uma declaração de ignorância. No futebol, se ganha e se perde por muito pouco, por um descuido, um suspiro, uma pausa na respiração e por tantos instantes fugazes.

A ciência não consegue medir a capacidade de observação de um treinador nem a lucidez, as emoções e os encantos dos atletas.

Conhecimento não é apenas informação. Muitos técnicos possuem todos os dados estatísticos e científicos, mas não são bons observadores, especialmente dos detalhes subjetivos, do que não pode ser medido nem nunca será.

###### décimo nono dia da triplo x