«E enquanto outros, os professores da Sorbonne, os conselheiros, os legados, os zwínglios, os calvinos, proclamam: "Nós conhecemos a verdade", a resposta de Montaigne é "Que sei eu?"»

###### trigésimo dia da vitória de pirro