# quem se desloca recebe, quem pede tem preferência

Além de aceitar pagar a maior multa da Operação Lava-Jato, a empreiteira Andrade Gutierrez acertou um acordo de delação com a Procuradoria Geral da República e da força-tarefa de procuradores e policiais que atua em Curitiba (PR), no qual irá relatar que pagou propina em obras da Copa do Mundo, na Petrobras, na usina nuclear Angra 3 e em Belo Monte e na ferrovia Norte-Sul. 

A indenização paga será de R$ 1 bilhão, cerca de R$ 200 milhões a mais que a Camargo Corrêa, cuja multa foi de R$ 800 milhões. O valor visa ressarcir as empresas que foram prejudicadas por acertos do cartel que atua em obras públicas.

###### terceiro dia de passe livre