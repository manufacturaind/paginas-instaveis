# tio rei explica o olavismo

O “olavismo” — uma referência ao filósofo de si mesmo Olavo de Carvalho, também conhecido como “Aiatolavo” — é uma forma de obscurantismo. Seus seguidores aprendem a obedecer, não a pensar. No mais das vezes, são de uma ignorância profunda e arrogante. Não chegam a encantar pela inocência porque Olavo degrada tudo aquilo em que toca.

O que lhes resta de pensamento apodrece muito rapidamente, corroído pelo preconceito e por teorias conspiratórias as mais exóticas. Olavo é um Jim Jones malsucedido. Mas envenena, sim, algumas pobres almas.

Num dos posts que escrevi aqui, chamei seus seguidores de “feios, sujos e malvados”. Em italiano, fica mais sonoro: “brutti, sporchi e cattivi”.

Qual e o segredo do Curso Madureza de Filosofia do “mestre”? Transmitir a rematados ignorantes a sensação de sabedoria. Olavo lustra os preconceitos de gente brucutu, empresta a pensamentos os mais rombudos a aparência de profundidade e alimenta a autossuficiência dos estúpidos. Uma vez reconhecidos pelo “mestre”, é como se galgassem postos na Seita Olaviana.

###### trigésimo sexto dia da vitória de pirro