# da brazilians

Pense em mim como Gabriel, só que com mais beleza

O vice suporta todos seus esforços, este é só o início

Você está apenas no início

Por que faríamos isso? Pra justificar o golpe?

Nunca aconteceu pra gente, mas sinto que está acontecendo agora

O filho do deputado foi encontrado com três quilos de marijuana

Você sabe o que fazem com traficantes no Brasil?

Yah, yah

Os deputados estão reunidos com o staff do vice

A presidente será atingida

Por quê? Por nos ter feito tirar a manhã de folga?

Nós tivemos a nossa quota de crises políticas

Se aquele fiadaputa errou uma conjugação verbal nos últimos 10 anos, nós vamos descobrir onde e quando

Precisamos de um carro do governo

É mais fácil cortar um dedo dele

Isto já foi feito

Este leme vai ficar na mão do vice

O capitão saiu para o almoço e os ratos tomaram conta do navio

Estou em contato constante com o vice, se algo acontecer, eu aviso ele

Sei dos riscos, ou melhor, nesta altura já nem podemos imaginar

Será breve se não formos cuidadosos

Entendo, senhor, mas ela tem vivido a vida numa moldura bem estreita

Você acredita que as notícias são verdadeiras?

Em um dia como hoje, pode haver um monte de mal-entendidos e desinformação, e é nosso trabalho estarmos absolutamente certos de que o vice-presidente sabe o que está acontecendo

Você pode obter a história verdadeira? Não hoje

Não há nada melhor do que um ex-socialista-que-virou-conservador inflamar o clamor público sobre os males do comunismo, mas eu ainda sou um membro da imprensa

Me dê os nomes

Sinceramente, espero que este fiadaputa sangre até morrer nas próximas eleições

Seu comprometimento com a causa sempre foi total

Sem tempo para sinais hoje, meu chapa

Ouvi muitas coisas a seu respeito, penso que faremos grandes coisas em dupla

Gostaria que não tivéssemos começado desse jeito

Se nós fizermos isso ou não, eles vão jogar nas nossas costas da mesma forma

Eu lutei nas fileiras inimigas de Niemeyergrado por dois anos

Não tive a sua experiência, eu estava nas colunas presteslândicas muito tempo antes de você nascer

Hoje não é um bom dia, não estou disponível pra essas merdas

O supervisor disse que há um problema com a linha da morte no calendário

Primeiro de tudo, temos de descobrir de fato o que acontece na sala da presidência

Como deveríamos te chamar? De Claudia?

Claudia está bom

Aham, Claudia, senta lá

Você pode ouvir? O som da chapa esquentando algo no carrossel esta noite?

Estamos bem aliviados de ouvir isso

Estamos num tempo perigoso da história humana, e um momento como este pode mudar tudo

Os eventos de hoje não precisam ser mais politizados do que já foram

Aqui está um botton, cortesia do escritório do vice

Eu não acho, razoavelmente falando, que alguém acredite nessa história de que qualquer um pode assumir o governo

Nós podemos checar no site?

Você pode ficar comigo?

Ficar com você e o quê? Nós estamos no meio de uma crise!

Ya, e você não pensa que estão dando importância demais pra essa merda toda?

Constitucionalmente o vice é a pessoa a seguir na linha sucessória da presidência

Talvez seja legal, o que reflete a realidade política de tudo isso

Ah, sim, vamos relaxar

Acredito que os brasileiros são capazes de qualquer coisa

Você ouve o que eles dizem? O que dizem sobre nós? Você ouve a si mesmo quando fala deles? O que isto significa? Porra nenhuma

Sei como os brasileiros fazem as coisas, este não é um golpe

Ah, não, eles não mentem e não conspiram como todo mundo? Por que você pensa que eles são tão puros e tão diferentes de todos os outros?

Eu te prometo: tudo vai ficar bem

Do que mais você precisa, de um manifesto escrito? Tem algum disponível? Faça-me o favor

Verba volant, scripta manent

Esta porra saiu do controle

Um erro, um errinho de nada, e tudo começa

Ya

É apenas a história repetindo a si mesma, não precisamos nos preocupar

Ok, sinto que preciso de uma bola de cristal pra continuar esta conversa