# montaigne escreve sobre futebol

Por causa de grandes craques da história, que executavam belíssimas jogadas individuais, e do enorme desenvolvimento da tecnologia e da comunicação, criou-se, nos últimos tempos, uma cultura esportiva de supervalorizar os lances individuais e isolados, os melhores momentos, as imagens, que são mostradas e discutidas milhões de vezes nos programas esportivos, em todos os ângulos, como se fossem uma síntese da partida. Pouco se fala do jogo coletivo.

Muitos comentaristas acham que todos os lances isolados ou um gol têm uma explicação técnica, com movimentos planejados. Não é bem assim. As decisões, no instante do lance, costumam ser circunstanciais, intuitivas, decididas no momento. Isso é muito diferente do jogo estruturado, coletivo, de uma área à outra.

Na Copa de 1970, o cineasta Pasolini escreveu que a poesia brasileira, dos lances individuais, inventados e surpreendentes, derrotou a prosa italiana, do jogo coletivo, lógico, estruturado, com um meio e um fim. Não foi bem assim. Foi uma vitória da poesia e da prosa brasileiras, da forma e do conteúdo, da invenção e da razão. O futebol tem lógica, porém as lógicas são muitas.

###### décimo primeiro dia da conexão mônaco